//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/NotFlix?replicaSet=rs0';

setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var videoSchema = new Schema({
  accountID: Number,
  username: String,
  title: String,
  userAction: String,
  dateAndTime: String,
  pointOfInteraction: String,
  typeOfInteraction: String
});

var videoModel = mongoose.model('video', videoSchema, 'video');



app.get('/', (req, res) => {
  videoModel.find({},'accountID username title userAction dateAndTime pointOfInteraction typeOfInteraction', (err, video) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(video))
  }) 
})

app.post('/',  (req, res) => { 
  var new_video_instance = new videoModel(req.body); 
  new_video_instance.save(function (err) { 
  if (err) res.send('Error'); 
    res.send(JSON.stringify(req.body)) 
  }); 
});

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

//SECTION A
//Get the hostname of the node and set variables
var os = require("os");
var myhostname = os.hostname();
var maxNodeID = 0;
systemLeader = 0
var time;
let node = [];
var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = {"hostname" : myhostname, "status": "alive","nodeID":nodeID} ;

//route for get page /
app.get('/', (req, res) => {
 //Send the response to the browser
  res.send("<html> <body bgcolour=" + ((leader)? "#FF0000":"#00FF00") + "> Hello this is node " + myhostname + ((leader)? "I am the leader :-)":"I am not the leader :-(") + "</body></html>");
})

//this function waits for a message from the message queue and receives
function a(){
//subscription needs to run once
var amqp = require('amqplib/callback_api');

amqp.connect('amqp://test:test@cloud-coursework_haproxy_1', function(error0, connection) {
    if (error0) {
        throw error0;
      }
connection.createChannel(function(error1, channel) {
        if (error1) {
                  throw error1;
                }
        var exchange = 'logs';

        channel.assertExchange(exchange, 'fanout', {
                  durable: false
                });

        channel.assertQueue('', {
                  exclusive: true
                }, function(error2, q) {
                          if (error2) {
                                      throw error2;
                                    }
                          console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                          channel.bindQueue(q.queue, exchange, '');

                          channel.consume(q.queue, function(msg) {
                                      if(msg.content) {
                                        let date = new Date();
                                        let t = -(time - date.getTime());
                                        let lastMsg = t / 1000;
                                        let myhostname = JSON.parse(msg.content.toString("utf-8")).hostname;
                                        let nodeID = JSON.parse(msg.content.toString("utf-8")).nodeID;
                                        console.log(myhostname + "[x] received message from  %s", msg.content.toString());
                                        node.some(node => node.hostname === myhostname) ? (node.find(e => e.hostname === myhostname)).lastMessage = lastMsg : node.push({"hostname":myhostname,"nodeID":nodeID, "lastMessage":lastMsg, "isAlive" : "alive"});

                                                    }
                                    }, {
                                                noAck: true
                                              });
                        });
      });

});
}
setInterval(function() {

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://test:test@cloud-coursework_haproxy_1', function(error0, connection) {
     //if connection failed throw error
    if (error0) {
        throw error0;
    }

    //create a channel if connected and send hello world to the logs Q
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'logs';
        var msg =  JSON.stringify(toSend);

        if (nodeID > maxNodeID){
          maxNodeID = nodeID;
        }
        
        channel.assertExchange(exchange, 'fanout', {
                durable: false
        });
        
        channel.publish(exchange, '', Buffer.from(msg));
        console.log(myhostname + " [x] Sent %s", msg);
     });

           
     //in 1/2 a second force close the connection
     setTimeout(function() {
         connection.close();
     }, 500);

});
}, 3000);

//SECTION B
//check if leader
setInterval(function() {
  leader = 1;
  activeNodes = 0;
  var maxId = 0;
  var maxHostName = "";
  
  Object.entries(node).forEach((n) => {
  console.log(n);
  
  if(maxId < n[1].nodeID){
          maxId = n[1].nodeID;  
          maxHostName = "leader is - " + n;      
        }
  });

  console.log(maxHostName);   
}, 2000);
setTimeout(a, 10000);

//SECTION C
// restarts containers for HA
//async function createContainer(){
  //  try{
    //    await axios.post(`http://host.docker.internal:2375/containers/create?name=${containerName}`, containerDetails).then(function(response){console.log(response)});
      //  await axios.post(`http://host.docker.internal:2375/containers/${containerName}/start`);
    //}
    //catch(error)
    //{
     //   console.log(error);
    //}
//}

//createContainer();


